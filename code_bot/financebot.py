import json
import sys
import os
import re
import uuid
import prettytable
import asyncpg
import aiohttp
import asyncio

from datetime import datetime, timedelta
from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import ParseMode
from threading import Thread

from avgDailyMoneySpend import DailyMoneySpend

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

with open(f"{BASE_DIR}/env.json", 'r') as settings:
    settings = json.loads(settings.read())

if not settings:
    print("smth wrong with env.json file!")
    sys.exit(1)

db, curr = (None, None)

BOT_TOKEN = settings.get("bot-token", None)
PROXY_URL = settings.get("proxystring", None)
PROXY_AUTH = aiohttp.BasicAuth(
    login=settings.get("proxyuser"), 
    password=settings.get("proxypassword")
)
bot = Bot(token=BOT_TOKEN, proxy=PROXY_URL, proxy_auth=PROXY_AUTH)
dp = Dispatcher(bot)


async def initizalite_database():
    '''
    Процедура устанавливает соединение с базой данных
    '''
    print('trying to connect to postgres... ', end='')
    global db, curr
    while True:
        try:
            db = await asyncpg.connect(
                host='db', 
                database='financebot', 
                user=settings.get('pguser', None), 
                password=settings.get('pgpassword', None)
            )
            print('ok!')
            break
        except:
            await asyncio.sleep(3)


async def get_separated_text(text: str, gap=10) -> str:
    '''
    Функция преобразует текст в столбик,
    пытаясь равномерно переносить слова
    :param text:
        Строка с исходным текстом
    :param gap:
        Ширина колоки, под которую подстроится текст
    :return:
        Двумерный массив со строками и словами 
    '''
    def array_sum_elem(ar):
        return sum([len(t) for t in ar]) + len(ar) - 1
    
    mas = text.split(' ')
    result = []
    line = []
    for item in mas:
        if len(item) > gap:
            while len(item) > gap:
                result.append([item[:gap]])
                item = item[gap:]
            line = [item]
        else:
            line.append(item)
            if array_sum_elem(line) > gap:
                if len(line[-1]) > gap:
                    s = line[-1]
                    t = gap - array_sum_elem(line[:-1])
                    line[-1] = line[-1][:t]
                    result.append(line)
                    line = [s[t:]]
                elif len(line[-1]) <= gap:
                    s = line.pop()
                    result.append(line)
                    line = [s]

    else:
        result.append(line)
        
    return '\n'.join([' '.join(item) for item in result])


async def get_sum_for_message(_sum: float, no_bold=False, absolute=False) -> str:
    '''
    Функция преобразовывает числовую сумму в тестовый вариант, добавляя
    "руб." в конец и выделяя жирным шрифтом, если необходимо
    :param _sum:
        Числовая сумма
    :param no_bold:
        Флаг, отвечающий за стиль текста (жирный / нежирный)
    :param absolute:
        Флаг, означающий преобразование суммы к абсолютному значению
    '''
    if not _sum:
        _sum = 0
    if _sum == int(_sum):
        _sum = int(_sum)
    
    if absolute:
        _sum = abs(_sum)

    reply = f"{_sum}"

    pattern = "%s" if no_bold else "<b>%s</b>" 
    return (pattern + " руб.") % reply
    

async def get_sum_from_match(reg_result: re.Match, positive_is_negative=False) -> float:
    '''
    Функция извлекает из группировки регулярного выражения сумму
    :param reg_result:
        re.Match - результат поиска по регулярному выражению  
    :param positive_is_negative:
        Флаг, определяющий, что входящее положительно число
        должно быть рассмотренно как отрицательное  
    '''
    num = reg_result.group('sum').replace(',', '.')
    grades = num.split('.')
    if abs(float(grades[0])) > 10 ** 9 - 1:
        return 0
    hardcoded_positive = "+" in num
    result = round(float(num), 2)
    if positive_is_negative and result > 0 and not hardcoded_positive:
        result = -result    
    return result


async def daily_income_check():
    '''
    Циклическая процедура, начисляющая сумму на ежедневный счет
    каждый день
    '''
    while True:
        now = datetime.now()
        daily_entries = {}
        result = await db.fetch('''
            SELECT id, value, last_income 
            FROM daily_amount 
            WHERE value != 0
        ''')
        for line in result:
            _id, value, last_income = line
            last_income = datetime.fromtimestamp(last_income)
            days_skipped = (now - last_income).days

            if days_skipped == 0 and now.day != last_income.day:
                days_skipped = 1

            if days_skipped == 0:
                continue

            new_element = {
                'value': value * days_skipped,
                'days_skipped': days_skipped                            
            }       
            daily_entries[_id] = new_element
            await register_record(
                _id, 
                new_element['value'], 
                "dincome", 
                notification=False
            )
        query = '''
            UPDATE daily_amount 
            SET last_income=$1 
            WHERE id=$2
        '''
        if len(daily_entries.keys()):
            await db.executemany(
                query,
                list(map(lambda k: [now.timestamp(), k], daily_entries.keys()))
            )   

        await asyncio.sleep(60)


async def add_saved_money(_id: int, _sum: float, comment: str):
    '''
    Процедура, регистрирующая поступление или изъятие средств
    с сберегательного счета
    :param _id:
        Идентификатор пользователя telegram
    :param _sum:
        Сумма начисления / списания
    :param comment:
        Комментарий к денежной транзакции
    '''
    if _sum < 0:
        already_saved = await get_money_remains(_id, _table="SAVED")
        if already_saved < _sum:
            reply = "SAVED: Вы пытаетесь списать %s, но доступно к списанию только %s" % (
                await get_sum_for_message(_sum),
                await get_sum_for_message(already_saved)    
            )
            await bot.send_message(
                _id, 
                "%s%s" % (reply, "\nТранзакция не выполнена"), 
                parse_mode=ParseMode.HTML
            )
            return
    await register_record(_id, _sum, source="SAVED", comment=comment)


async def show_all_remains(_id: int):
    '''
    Процедура получения дампа данных по счетам
    '''
    reply = ''
    for table in ["DAILY", "SAVED"]:
        reply += f"{table} {await get_remains_with_string(_id, table)}\n"
    reply = reply[:-1]
    await bot.send_message(_id, reply, parse_mode=ParseMode.HTML)


async def add_update_daily(_id: int, _sum: float):
    '''
    Процедура установки ежедневного автоматического прихода
    денежных средств на ежедневный счет
    :param _id:
        Идентификатор пользователя telegram
    :param _sum:
        Сумма ежедневного начисления
    '''
    result = await db.fetchrow('''
        SELECT value 
        FROM daily_amount 
        WHERE id=$1
    ''', _id)

    if result:
        query = '''
            UPDATE daily_amount 
            SET value=$1 
            WHERE id=$2
        '''
        await db.fetch(query, _sum, _id)
    else:
        query = '''
            INSERT INTO daily_amount 
            VALUES ($1, $2, $3)
        '''
        await db.fetch(query, _id, _sum, datetime.now().timestamp())

    reply = "Ежедневый приход был %s на сумму %s" % (
        "обновлен" if result else "добавлен",
        await get_sum_for_message(_sum)
    )
    await bot.send_message(_id, reply, parse_mode=ParseMode.HTML)


async def register_record(_id: int, _sum: float, 
                          comment: str="", source: str="DAILY", 
                          notification:bool =True):
    '''
    Процедура регистрации новой записи движения денежных средств
    :papam _id:
        Идентификатор пользователя telegram
    :param _sum:
        Сумма начисления / списания
    :param comment:
        Комментарий к денежной транзакции
    :param source:
        Разрез (счет) регистрации
    :param notification:
        Флаг, отражающий отправку пользователю ответного сообщения
    '''
    if not _sum:
        return

    comment = "" if comment is None else comment
    current_time = datetime.now().timestamp()
    query = '''
        INSERT INTO finance (date, id, value, uid, comment, source) 
        VALUES ($1, $2, $3, $4, $5, $6)
    '''
    await db.fetch(
        query, current_time, _id, _sum, str(uuid.uuid4()), comment, source)

    if notification:
        reply = "%s: было %s %s\n%s" % (
            source.upper(),
            "зачислено" if _sum > 0 else "списано",
            await get_sum_for_message(_sum, absolute=True), 
            await get_remains_with_string(_id, _table=source)     
        )
        await bot.send_message(_id, reply, parse_mode=ParseMode.HTML)


async def get_remains_with_string(_id: int, _table: str) -> str:
    '''
    Функция возвращает строку с текстом и 
    остатком по определенному счету
    :param _id:
        Идентификатор пользователя telegram
    :param _table:
        Разрез (счет) остатков
    '''
    remains = await get_money_remains(_id, _table=_table)
    return f"Остаток: {await get_sum_for_message(remains)}"


async def get_money_remains(_id: int, _date=None, _table="DAILY") -> float:
    '''
    Функция возвращает остаток в виде числа по требуемому разрезу (счету)
    :param _id:
        Идентификатор пользователя telegram
    :param _date:
        Дата, на которую нужно получить остаток
    :param _table:
        Разрез (счет) остатков
    '''
    if not _date:
        _date = datetime(2999, 12, 31).timestamp()
    result = await db.fetchrow('''
        SELECT ROUND(SUM(value)::numeric, 2) AS value
        FROM finance
        WHERE id=$1 AND source=$2 AND date < $3
    ''', _id, _table, _date)

    return result['value'] if result and result['value'] else 0


async def cancel_last(_id):
    '''
    Процедура отменяет последнюю операцию пользователя
    :param _id:
        Идентификатор пользователя telegram
    '''
    last_entry = await db.fetchrow('''
        SELECT uid, value, source 
        FROM finance 
        WHERE id=$1 ORDER BY date DESC LIMIT 1
    ''', _id)

    if not last_entry:
        return None

    await db.fetch('''
        DELETE FROM finance 
        WHERE uid=$1 AND value=$2 AND source=$3
    ''', last_entry['uid'], last_entry['value'], last_entry['source'])

    reply = "%s: отменил транзакцию на %s\n%s" % (
        last_entry['source'].upper(),
        await get_sum_for_message(last_entry['value']),
        await get_remains_with_string(_id, _table=last_entry['source'])
    )
    await bot.send_message(_id, reply, parse_mode=ParseMode.HTML)


async def get_details(_id: int, _from: float, _to: float, source: str):
    '''
    Процедура вывода детализированного отчета по операциям за период
    по определенному счету
    :param _id:
        Идентификатор пользователя telegram
    :param _from:
        Слепок времени даты начала
    :param _to:
        Слепок времени даты окончания
    :param source:
        Разрез (счет) регистрации
    '''
    result = await db.fetch('''
        SELECT date, value, comment 
        FROM finance 
        WHERE id=$1 AND date BETWEEN $2 AND $3 AND source=$4
    ''', _id, _from, _to, source)

    if result:
        avg_counter = DailyMoneySpend()
        table = prettytable.PrettyTable(["Дата", "Сумма", "Коммент"])
        for line in result:
            query_date = datetime.fromtimestamp(line['date'])
            table.add_row(
                ["%d.%d" % (
                    query_date.day,
                    query_date.month
                ), "%s%d" % (
                    "+" if line['value'] > 0 else "",
                    line['value']   
                ),
                "%s" % await get_separated_text(line['comment'])]) 
            if line['value'] < 0 and source == "DAILY":
                avg_counter.add_date_fund(query_date, float(line['value']))
        reply = f'Детализация {source.upper()}:\n'
        if abs(_from - _to) <= 86400:
            reply += f'за {datetime.fromtimestamp(_from).strftime("%d %b %Y")}'
        else:
            reply += "с %s по %s" % (
                datetime.fromtimestamp(_from).strftime("%d %b %Y"),
                datetime.fromtimestamp(_to).strftime("%d %b %Y")
            )
        remains_from = await get_sum_for_message(await get_money_remains(_id, _from, source), True)
        remains_to = await get_sum_for_message(await get_money_remains(_id, _to, source), True)
        avg_spend_per_day = await get_sum_for_message(avg_counter.calculate_avg_all(round_to=0), True)
        reply += "\n<code>Остаток на начало: %s\n%s\nОстаток на конец: %s</code>" % (
            remains_from, str(table), remains_to
        )
        if avg_spend_per_day and source == "DAILY":
            reply += f'\n<code>Средняя трата в день: {avg_spend_per_day}</code>'
    else:
        reply = "Нет данных за этот период"    
    await bot.send_message(_id, reply, parse_mode=ParseMode.HTML)


async def perform_details(_id: int, period: str, _table: str="DAILY"):
    '''
    Процедура обслуживания сообщения пользователя о запросе детализации
    :param _id:
        Идентификатор пользователя telegram
    :param period:
        Строка из сообщения пользователя (exp. "Расход два дня")
    :param _table:
        Разрез (счет) регистрации
    '''
    current = datetime.now()
    _to = datetime(current.year, current.month, current.day, 23, 59, 59)
    _from = datetime(current.year, current.month, current.day)
    if "два дня".upper() in period:
        _from = _from - timedelta(days=1)
    elif "неделя".upper() in period:
        _from = _from - timedelta(days=7)
        
    await get_details(_id, _from.timestamp(), _to.timestamp(), _table)


async def perform_remains_info(_id: int, _table: str):
    '''
    Процедура обслуживания сообщения пользователя о запросе остатков
    :param _id:
        Идентификатор пользователя telegram
    :param _table:
        Разрез (счет) регистрации
    '''
    user_sum = await get_money_remains(_id, _table=_table)
    if user_sum is None:
        reply = "У нас нет данных по Вашему аккаунту"
    else:
        reply = "%s %.2f" % (
            {
                "DAILY": "Доступно к трате сегодня", 
                "SAVED": "Ваши сбережения составляют"
            }.get(_table),
            await get_sum_for_message(user_sum)       
        )    
    await bot.send_message(_id, reply, parse_mode=ParseMode.HTML)


async def get_info_text():
    '''
    Функция возвращает описание команд бота
    '''
    return  ("<b>###.##</b> - списать сумму с ежедневного счета\n"
            "\t<i>Пример:</i> 760\n\n"
            "<b>+###.##</b> - зачислить сумму на ежедневный счет\n"
            "\t<i>Пример:</i> +130\n\n"
            "<b>###.## АБВГ ДЕЖЗ</b> - списать трату с ежедневного счета с комментарием\n"
            "\t<i>Пример:</i> 186 пивко в дикси\n\n"
            "<b>+###.## АБВГ ДЕЖЗ</b> - зачислить сумму на ежедневный счет с комментарием\n"
            "\t<i>Пример:</i> +60 нашел на улице\n\n"
            "<b>C|Сейф|s|Save ###.## АБВГ ДЕЖЗ</b> - зачислить сумму на сберегательный счет с комментарием\n"
            "\t<i>Пример:</i> save 10000 на лечение зубов\n\n"
            "<b>C|Сейф|s|Save -###.## АБВГ ДЕЖЗ</b> - списать сумму со сберегательного счета с комментарием\n"
            "\t<i>Пример:</i> save -3200 штраф дтп\n\n"
            "<b>д|дейли|d|daily ###.##</b> - установить ежедневный приход в 8 утра\n"
            "\t<i>Пример:</i> дейли 500\n\n"
            "<b>дамп</b> - получить текущие остатки по обоим счетам\n\n"
            "<i>Напишите любой текст, чтобы получить меню</i>")


async def perform_standard_action(_id):
    '''
    Процедура вывода меню в клиенте для пользователя
    '''
    markup = types.ReplyKeyboardMarkup()
    item_remains = types.KeyboardButton('Остаток')
    item_saved = types.KeyboardButton('Схрон')
    item_cancel = types.KeyboardButton('Отменить')
    item_detail1 = types.KeyboardButton("Расход сегодня")
    item_detail2 = types.KeyboardButton("Расход два дня")
    item_detail3 = types.KeyboardButton("Расход неделя")
    item_save_detail1 = types.KeyboardButton("Сейф сегодня")
    item_save_detail2 = types.KeyboardButton("Сейф два дня")
    item_save_detail3 = types.KeyboardButton("Сейф неделя")
    markup.row(item_remains, item_saved, item_cancel)
    markup.row(item_detail1, item_detail2, item_detail3)
    markup.row(item_save_detail1, item_save_detail2, item_save_detail3)
    await bot.send_message(_id, "Ваше действие:", reply_markup=markup)


reg_start = r"^"
reg_command = r"(?P<command>[A-ZА-Я]+)"
reg_sum = r"(?P<sum>(\+|-)?(\d+(,|\.)\d+|(,|\.)\d+|\d+))"
reg_comment = r"(?P<comment>.+)"
reg_end = r"$"

reg_daily = "%s%s%s" % (
    reg_start,
    reg_sum,
    r'($|\s+' + reg_comment + r'$)'
)

reg_user_command = "%s%s%s%s" % (
    reg_start,
    reg_command,
    r'\s+' + reg_sum,
    r'($|\s+' + reg_comment + r'$)'
)


@dp.message_handler(commands=["start", "help"])
async def on_first_message(message: types.Message):
    await bot.send_message(
        message.from_user.id,
        "%s\n\n%s\n\n%s" % (
            "Я прикольный финансовый бот!",
            "Мои команды:",
            await get_info_text()
        ),
        parse_mode=ParseMode.HTML
    )


@dp.message_handler(commands=["cancel"])
async def on_cancel_message(message: types.Message):
    await cancel_last(message.from_user.id)


@dp.message_handler()
async def on_message(message: types.Message):

    content = message.text

    if content.upper().startswith("Остаток".upper()):
        await perform_remains_info(message.from_user.id, "DAILY")
        return

    if content.upper().startswith("Отменить".upper()):
        await on_cancel_message(message)
        return

    if content.upper().startswith("Расход".upper()):
        await perform_details(
            message.from_user.id,
            " ".join(content.upper().split(" ")[1:])
        )
        return

    if content.upper().startswith("Схрон".upper()):
        await perform_remains_info(message.from_user.id, "SAVED")
        return

    if content.upper().startswith("Сейф".upper()):
        await perform_details(
            message.from_user.id,
            " ".join(content.upper().split(" ")[1:]),
            "SAVED"
        )
        return

    if content.upper().startswith("Дамп".upper()):
        await show_all_remains(message.from_user.id)
        return

    matches = re.search(reg_user_command, content, re.IGNORECASE)

    if matches:

        command = matches.group('command').upper()
        user_sum = await get_sum_from_match(matches)

        if command in ['D', 'Д', 'DAILY', "DAY", "ДЕЙЛИ", "ДЕНЬ"]:
            await add_update_daily(message.from_user.id, user_sum)

        elif command in ['С', 'S', 'SAVE', 'СЕЙФ']:
            comment = matches.group('comment')
            await add_saved_money(message.from_user.id, user_sum, comment)

        return

    matches = re.search(reg_daily, content, re.IGNORECASE)

    if matches:

        user_sum = await get_sum_from_match(matches, positive_is_negative=True)
        user_comment = matches.group('comment')

        await register_record(message.from_user.id, user_sum, user_comment)

        return

    await perform_standard_action(message.from_user.id)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(initizalite_database())
    asyncio.ensure_future(daily_income_check())
    print('bot polling starting..')
    executor.start_polling(dp, skip_updates=True)
