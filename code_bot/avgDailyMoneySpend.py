from datetime import datetime
from functools import reduce

class DailyMoneySpend:

    def __init__(self):
        self.data = {}

    def __get_key_date(self, date: datetime):
        return datetime(date.year, date.month, date.day)

    def add_date_fund(self, date: datetime, funds: float):
        key_date = self.__get_key_date(date)
        if self.data.get(key_date):
            self.data.get(key_date).append(funds)
        else:
            self.data[key_date] = [funds]

    def del_date(self, date: datetime):
        key_date = self.__get_key_date(date)
        self.data.pop(key_date, None)

    def calculate_avg_daily(self, round_to=2):
        return {k: round(sum(v) / len(v), round_to) for k, v in self.data.items()}

    def calculate_avg_all(self, round_to=2, absolute=True):
        if not self.data:
            return 0
        
        result_tuple = reduce(
            lambda prev, i: (prev[0] + i[0], prev[1] + i[1]), 
            [(sum(v), len(v) if len(self.data.keys()) == 1 else 1) for _, v in self.data.items()]
        )
        if round_to != 0:
            result = round(result_tuple[0] / result_tuple[1], round_to)
        else:
            result = round(result_tuple[0] / result_tuple[1])
        if absolute:
            return abs(result)
        else:
            return result
    