CREATE DATABASE financebot;
\c financebot;
CREATE TABLE finance (date NUMERIC, id INTEGER, value REAL, uid TEXT, comment TEXT, source TEXT);
CREATE TABLE daily_amount (id INTEGER, value REAL, last_income NUMERIC);